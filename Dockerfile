FROM mongo:4.4

# Create app directory
WORKDIR /init

ENV TZ=America/Montreal
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

COPY ./scripts/file.key .
COPY ./scripts/setup.sh .

# RUN 
RUN apt-get update && \
    apt-get install -y nano curl && \
    apt-get clean all && \
    apt-get -y autoremove --purge && \
    rm -rf /var/lib/apt/lists/*

RUN chmod 700 setup.sh
RUN chmod +x setup.sh
RUN chmod 400 file.key
