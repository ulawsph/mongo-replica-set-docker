# Mongo Replica Set Docker

Mini projet pour faire tourner un replica set Mongo dans un container local et pouvoir analyser les logs mongo. Il permet également de lancer et d'arrêter le container sans perdre les données persistées via la création d'un volume docker 'db-data'.

## 1. Création du replica set (Linux and Mac Os)

### 1.1 Se placer à la racine du projet

`cd mongo-replica-set-docker`  
Le script .envrc va s'executer pour ajouter les hostnames nécessaires au fonctionnement du replica set.

### 1.2 Exécuter la commande de lancement du replica set : (attendre la fin de l'exécution ~1min)

`sh launch.sh`  
Le script va exécuter les commandes suivantes :

1. `docker build -t mongo:rs .`
2. `docker-compose up -d`
3. `docker exec mongo-0.mongo /init/setup.sh`

La commande 1. va créer l'image Docker mongo qui sera utilisée pour créer les containers (la version 4.0 est utilisée par défaut).

La commande 2. va créer les 3 containers mongo qui formeront le replica set et les initialiser avec les bonnes configurations décritent dans les fichiers de conf (dossier /conf).

La commande 3. va exécuter le script setup.sh à l'interieur du container mongo-0.mongo qui a été configuré pour être le serveur PRIMARY du replica set.
Ce script va set up le replica set et créer les users 'admin' et 'clusterAdmin' pour la base de donnée 'admin'.

```
    admin.createUser(
    {
	    user: "admin",
        pwd: "password",
        roles: [ { role: "root", db: "admin" } ]
     });
    db.getSiblingDB("admin").auth("admin", "password");
    admin.createUser(
    {
	    user: "clusterAdmin",
        pwd: "password",
        roles: [ { role: "clusterAdmin", db: "admin" } ]
     });
```

Le mot de passe par défaut pour ces 2 users est 'password'.

Une section pour créer un autre user sur la base donnée spécifique à votre projet a été rajoutée pour exemple.

```
    use fincashingdepositapi
    db.createUser(
     {
	user: "test",
        pwd: "test",
        roles: [ { role: "readWrite", db: "fincashingdepositapi" } ]
     });
```

Mettez à jour le script selon votre besoin et projet.

## 2. Création du replica set (Windows)

### 2.1 Se placer à la racine du projet

`cd mongo-replica-set-docker`

### 2.1 Ajout des hostnames

Editer le ficher `hosts` dans `c:\Windows\System32\drivers\etc` en rajoutant les 3 lignes suivantes à la fin : (Administrateur)

127.0.0.1 mongo-0.mongo  
127.0.0.1 mongo-1.mongo  
127.0.0.1 mongo-2.mongo

### 2.2 Exécuter les commandes suivantes de lancement du replica set :

1. `docker build -t mongo:rs .`
2. `docker-compose up -d`
3. Attendre 30 sec le temps que les serveurs s'initialisent
4. `docker exec mongo-0.mongo /init/setup.sh`

(Voir les détails dans la section 1.2)

## 3. Stopper le replica set

`docker-compose down`

## 4. Regarder les logs du replica set

`docker-compose logs -f`

## 5. Connexion au Replica Set

`mongodb://admin:password@mongo-0.mongo:27017,mongo-1.mongo:27018,mongo-2.mongo:27019/?replicaSet=rs0`
