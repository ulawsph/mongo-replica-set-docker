#!/bin/sh

mongo <<EOF
   var cfg = {
        "_id": "rs0",
        "protocolVersion": 1,
        "members": [
            {
                "_id": 0,
                "host": "mongo-0.mongo:27017",
                "priority": 2
            },
            {
                "_id": 1,
                "host": "mongo-1.mongo:27018",
                "priority": 1
            },
            {
                "_id": 2,
                "host": "mongo-2.mongo:27019",
                "priority": 1
            }
        ]
    };
    rs.initiate(cfg, { force: true });
    rs.status();
EOF
sleep 20

mongo <<EOF
    use admin;
    admin = db.getSiblingDB("admin");
    admin.createUser(
    {
	    user: "admin",
        pwd: "password",
        roles: [ { role: "root", db: "admin" } ]
     });
    db.getSiblingDB("admin").auth("admin", "password");
    db.adminCommand({
        setDefaultRWConcern : 1,
        defaultWriteConcern : {
            "w" : "majority",
            "wtimeout" : 5000
        },
        defaultReadConcern : { "level" : "majority" }
    })
    admin.createUser(
    {
	    user: "clusterAdmin",
        pwd: "password",
        roles: [ { role: "clusterAdmin", db: "admin" } ]
     });
    use fincashingdepositapi
    db.createUser(
     {
	user: "test",
        pwd: "test",
        roles: [ { role: "readWrite", db: "fincashingdepositapi" } ]
     });
    use finarcollectionsapi
    db.createUser(
     {
	user: "test",
        pwd: "test",
        roles: [ { role: "readWrite", db: "finarcollectionsapi" } ]
     });
    use taxation
    db.createUser(
     {
	user: "test",
        pwd: "test",
        roles: [ { role: "readWrite", db: "taxation" } ]
     });
     rs.status();
EOF



