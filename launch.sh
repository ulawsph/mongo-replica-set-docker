#!/bin/bash

docker build --no-cache --tag="mongo:rs" .
docker-compose up -d
sleep 20
docker exec mongo-0.mongo ./setup.sh
